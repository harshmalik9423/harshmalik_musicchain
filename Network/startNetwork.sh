sh createCa.sh

sleep 5

echo "########## Setting required env ############"
export CHANNEL_NAME=musicchannel
export IMAGE_TAG=latest
export COMPOSE_PROJECT_NAME=musicxnetwork-ca

echo "########## Generate the genesis block ############"
configtxgen -profile OrdererGenesis \
-channelID system-channel -outputBlock \
./channel-artifacts/genesis.block

sleep 2

echo "########## Generate the Channel Transaction ############"
configtxgen -profile MusicChannel \
-outputCreateChannelTx ./channel-artifacts/$CHANNEL_NAME.tx \
-channelID $CHANNEL_NAME

sleep 2

echo "########## Starting the components ############"
docker-compose -f docker/docker-compose-singlepeer.yml up -d

export ORDERER_TLS_CA=`docker exec cli  env | grep ORDERER_TLS_CA | cut -d'=' -f2`

sleep 2

echo "########## Creating the Channel ############"
docker exec cli peer channel create -o orderer.musicx.com:7050 \
-c $CHANNEL_NAME -f /opt/gopath/src/github.com/hyperledger/fabric/peer/config/$CHANNEL_NAME.tx \
--tls --cafile $ORDERER_TLS_CA


sleep 2

echo "########## Joining Artist Peer to Channel ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=ArtistMSP \
     -e CHANNEL_NAME=musicchannel \
     -e CORE_PEER_ADDRESS=peer0.artist.musicx.com:7051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/musicx.com/orderer.musicx.com/msp/tlscacerts/tlsca.musicx.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/musicx.com/artist.musicx.com/peers/peer0.artist.musicx.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/musicx.com/artist.musicx.com/peers/peer0.artist.musicx.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/musicx.com/artist.musicx.com/peers/peer0.artist.musicx.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/musicx.com/artist.musicx.com/users/Admin@artist.musicx.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer channel join -b $CHANNEL_NAME.block

sleep 2

echo "########## Joining Listener Peer to Channel ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=ListenerMSP \
     -e CHANNEL_NAME=musicchannel \
     -e CORE_PEER_ADDRESS=peer0.listener.musicx.com:9051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/musicx.com/orderer.musicx.com/msp/tlscacerts/tlsca.musicx.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/musicx.com/listener.musicx.com/peers/peer0.listener.musicx.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/musicx.com/listener.musicx.com/peers/peer0.listener.musicx.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/musicx.com/listener.musicx.com/peers/peer0.listener.musicx.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/musicx.com/listener.musicx.com/users/Admin@listener.musicx.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer channel join -b $CHANNEL_NAME.block

sleep 2

echo "########## Generating anchor peer tx for artist ############"
configtxgen -profile MusicChannel -outputAnchorPeersUpdate ./channel-artifacts/ArtistMSPanchors.tx -channelID musicchannel -asOrg ArtistMSP

sleep 2

echo "########## Generating anchor peer tx for listener ############"
configtxgen -profile MusicChannel -outputAnchorPeersUpdate ./channel-artifacts/ListenerMSPanchors.tx -channelID musicchannel -asOrg ListenerMSP

sleep 2

# echo "########## Generating anchor peer tx for listener ############"
# configtxgen -profile MusicChannel -outputAnchorPeersUpdate ./channel-artifacts/MvdMSPanchors.tx -channelID musicchannel -asOrg MvdMSP

# sleep 2

echo "########## Anchor Peer Update for Artist ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=ArtistMSP \
     -e CHANNEL_NAME=musicchannel \
     -e CORE_PEER_ADDRESS=peer0.artist.musicx.com:7051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/musicx.com/orderer.musicx.com/msp/tlscacerts/tlsca.musicx.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/musicx.com/artist.musicx.com/peers/peer0.artist.musicx.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/musicx.com/artist.musicx.com/peers/peer0.artist.musicx.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/musicx.com/artist.musicx.com/peers/peer0.artist.musicx.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/musicx.com/artist.musicx.com/users/Admin@artist.musicx.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer channel update -o orderer.musicx.com:7050 -c musicchannel -f ./config/ArtistMSPanchors.tx --tls --cafile $ORDERER_TLS_CA

sleep 2

echo "##########  Anchor Peer Update for listener ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=ListenerMSP \
     -e CHANNEL_NAME=musicchannel \
     -e CORE_PEER_ADDRESS=peer0.listener.musicx.com:9051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/musicx.com/orderer.musicx.com/msp/tlscacerts/tlsca.musicx.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/musicx.com/listener.musicx.com/peers/peer0.listener.musicx.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/musicx.com/listener.musicx.com/peers/peer0.listener.musicx.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/musicx.com/listener.musicx.com/peers/peer0.listener.musicx.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/musicx.com/listener.musicx.com/users/Admin@listener.musicx.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer channel update -o orderer.musicx.com:7050 -c musicchannel -f ./config/ListenerMSPanchors.tx --tls --cafile $ORDERER_TLS_CA

sleep 2

echo "##########  Package Chaincode ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=ArtistMSP \
     -e CHANNEL_NAME=musicchannel \
     -e CORE_PEER_ADDRESS=peer0.artist.musicx.com:7051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/musicx.com/orderer.musicx.com/msp/tlscacerts/tlsca.musicx.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/musicx.com/artist.musicx.com/peers/peer0.artist.musicx.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/musicx.com/artist.musicx.com/peers/peer0.artist.musicx.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/musicx.com/artist.musicx.com/peers/peer0.artist.musicx.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/musicx.com/artist.musicx.com/users/Admin@artist.musicx.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer lifecycle chaincode package harshmusicx.tar.gz --path /opt/gopath/src/github.com/chaincode/Music-smart-contract/ --lang node --label harshmusicx_1

sleep 2

echo "##########  Install Chaincode on Artist peer ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=ArtistMSP \
     -e CHANNEL_NAME=musicchannel \
     -e CORE_PEER_ADDRESS=peer0.artist.musicx.com:7051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/musicx.com/orderer.musicx.com/msp/tlscacerts/tlsca.musicx.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/musicx.com/artist.musicx.com/peers/peer0.artist.musicx.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/musicx.com/artist.musicx.com/peers/peer0.artist.musicx.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/musicx.com/artist.musicx.com/peers/peer0.artist.musicx.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/musicx.com/artist.musicx.com/users/Admin@artist.musicx.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer lifecycle chaincode install harshmusicx.tar.gz


sleep 2

echo "##########  Install Chaincode on Listener peer ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=ListenerMSP \
     -e CHANNEL_NAME=musicchannel \
     -e CORE_PEER_ADDRESS=peer0.listener.musicx.com:9051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/musicx.com/orderer.musicx.com/msp/tlscacerts/tlsca.musicx.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/musicx.com/listener.musicx.com/peers/peer0.listener.musicx.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/musicx.com/listener.musicx.com/peers/peer0.listener.musicx.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/musicx.com/listener.musicx.com/peers/peer0.listener.musicx.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/musicx.com/listener.musicx.com/users/Admin@listener.musicx.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer lifecycle chaincode install harshmusicx.tar.gz

sleep 2

echo "##########  Copy the above package ID for next steps, follow the approveCommit.txt ############"
