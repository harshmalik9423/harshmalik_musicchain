echo "##########Strating the docker-compose-ca############"
export COMPOSE_PROJECT_NAME=fabricscratch-ca
docker-compose -f ./docker/docker-compose-ca.yml up -d 

echo "##########Creating the folder structure for CA############"

mkdir -p organizations/peerOrganizations/musicx.com/artist.musicx.com/
mkdir -p organizations/peerOrganizations/musicx.com/listener.musicx.com/

echo "##########Setting PATH for Fabric CA client############"

export FABRIC_CA_CLIENT_HOME=${PWD}/organizations/peerOrganizations/musicx.com/artist.musicx.com/
sleep 2

echo "########## Enroll artist ca admin############"

fabric-ca-client enroll -u https://admin:adminpw@localhost:7054 --caname ca-artist --tls.certfiles ${PWD}/organizations/fabric-ca/artist/tls-cert.pem

sleep 5

echo "########## Create config.yaml for artist ############"

echo 'NodeOUs:
  Enable: true
  ClientOUIdentifier:
    Certificate: cacerts/localhost-7054-ca-artist.pem
    OrganizationalUnitIdentifier: client
  PeerOUIdentifier:
    Certificate: cacerts/localhost-7054-ca-artist.pem
    OrganizationalUnitIdentifier: peer
  AdminOUIdentifier:
    Certificate: cacerts/localhost-7054-ca-artist.pem
    OrganizationalUnitIdentifier: admin
  OrdererOUIdentifier:
    Certificate: cacerts/localhost-7054-ca-artist.pem
    OrganizationalUnitIdentifier: orderer' > ${PWD}/organizations/peerOrganizations/musicx.com/artist.musicx.com/msp/config.yaml

echo "########## Register artist peer ############"
fabric-ca-client register --caname ca-artist --id.name peer0artist --id.secret peer0pw --id.type peer --tls.certfiles ${PWD}/organizations/fabric-ca/artist/tls-cert.pem
sleep 5

echo "########## Register artist user ############"

fabric-ca-client register --caname ca-artist --id.name user1 --id.secret user1pw --id.type client --tls.certfiles ${PWD}/organizations/fabric-ca/artist/tls-cert.pem
sleep 5

echo "########## Register artist Org admin ############"

fabric-ca-client register --caname ca-artist --id.name artistadmin --id.secret artistadminpw --id.type admin --tls.certfiles ${PWD}/organizations/fabric-ca/artist/tls-cert.pem
sleep 5

echo "########## Generate artist peer MSP ############"

fabric-ca-client enroll -u https://peer0artist:peer0pw@localhost:7054 --caname ca-artist -M ${PWD}/organizations/peerOrganizations/musicx.com/artist.musicx.com/peers/peer0.artist.musicx.com/msp --csr.hosts peer0.artist.musicx.com --tls.certfiles ${PWD}/organizations/fabric-ca/artist/tls-cert.pem
sleep 5

echo "########## Generate artist Peer tls cert ############"

fabric-ca-client enroll -u https://peer0artist:peer0pw@localhost:7054 --caname ca-artist -M ${PWD}/organizations/peerOrganizations/musicx.com/artist.musicx.com/peers/peer0.artist.musicx.com/tls --enrollment.profile tls --csr.hosts peer0.artist.musicx.com --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/artist/tls-cert.pem
sleep 5

echo "########## Organizing the folders ############"
echo "########## Copy the certificate files ############"

cp organizations/peerOrganizations/musicx.com/artist.musicx.com/peers/peer0.artist.musicx.com/tls/tlscacerts/* organizations/peerOrganizations/musicx.com/artist.musicx.com/peers/peer0.artist.musicx.com/tls/ca.crt
cp organizations/peerOrganizations/musicx.com/artist.musicx.com/peers/peer0.artist.musicx.com/tls/signcerts/* organizations/peerOrganizations/musicx.com/artist.musicx.com/peers/peer0.artist.musicx.com/tls/server.crt
cp organizations/peerOrganizations/musicx.com/artist.musicx.com/peers/peer0.artist.musicx.com/tls/keystore/* organizations/peerOrganizations/musicx.com/artist.musicx.com/peers/peer0.artist.musicx.com/tls/server.key

mkdir -p ${PWD}/organizations/peerOrganizations/musicx.com/artist.musicx.com/msp/tlscacerts
cp ${PWD}/organizations/peerOrganizations/musicx.com/artist.musicx.com/peers/peer0.artist.musicx.com/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/musicx.com/artist.musicx.com/msp/tlscacerts/ca.crt

mkdir -p ${PWD}/organizations/peerOrganizations/musicx.com/artist.musicx.com/tlsca
cp ${PWD}/organizations/peerOrganizations/musicx.com/artist.musicx.com/peers/peer0.artist.musicx.com/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/musicx.com/artist.musicx.com/tlsca/tls-localhost-7054-ca-artist.pem

mkdir -p ${PWD}/organizations/peerOrganizations/musicx.com/artist.musicx.com/ca
cp ${PWD}/organizations/peerOrganizations/musicx.com/artist.musicx.com/peers/peer0.artist.musicx.com/msp/cacerts/* ${PWD}/organizations/peerOrganizations/musicx.com/artist.musicx.com/ca/localhost-7054-ca-artist.pem

cp organizations/peerOrganizations/musicx.com/artist.musicx.com/msp/config.yaml organizations/peerOrganizations/musicx.com/artist.musicx.com/peers/peer0.artist.musicx.com/msp/config.yaml

echo "Generate User MSP"
echo "================="

fabric-ca-client enroll -u https://user1:user1pw@localhost:7054 --caname ca-artist -M ${PWD}/organizations/peerOrganizations/musicx.com/artist.musicx.com/users/User1@artist.musicx.com/msp --tls.certfiles ${PWD}/organizations/fabric-ca/artist/tls-cert.pem

sleep 5

cp organizations/peerOrganizations/musicx.com/artist.musicx.com/msp/config.yaml organizations/peerOrganizations/musicx.com/artist.musicx.com/users/User1@artist.musicx.com/msp/config.yaml

echo "Generate org Admin MSP"
echo "======================"

fabric-ca-client enroll -u https://artistadmin:artistadminpw@localhost:7054 --caname ca-artist -M ${PWD}/organizations/peerOrganizations/musicx.com/artist.musicx.com/users/Admin@artist.musicx.com/msp --tls.certfiles ${PWD}/organizations/fabric-ca/artist/tls-cert.pem
sleep 5

cp ${PWD}/organizations/peerOrganizations/musicx.com/artist.musicx.com/msp/config.yaml ${PWD}/organizations/peerOrganizations/musicx.com/artist.musicx.com/users/Admin@artist.musicx.com/msp/config.yaml

echo "============================================================End of artist =================================================================================================="

echo "================================================================== listener =================================================================================================="


export FABRIC_CA_CLIENT_HOME=${PWD}/organizations/peerOrganizations/musicx.com/listener.musicx.com/

echo "enroll listener ca"
echo "======================"

fabric-ca-client enroll -u https://admin:adminpw@localhost:8054 --caname ca-listener --tls.certfiles ${PWD}/organizations/fabric-ca/listener/tls-cert.pem

sleep 5




echo "Create config.yaml for listener" 
echo "==================================="

  echo 'NodeOUs:
  Enable: true
  ClientOUIdentifier:
    Certificate: cacerts/localhost-8054-ca-listener.pem 
    OrganizationalUnitIdentifier: client
  PeerOUIdentifier:
    Certificate: cacerts/localhost-8054-ca-listener.pem
    OrganizationalUnitIdentifier: peer
  AdminOUIdentifier:
    Certificate: cacerts/localhost-8054-ca-listener.pem
    OrganizationalUnitIdentifier: admin
  OrdererOUIdentifier:
    Certificate: cacerts/localhost-8054-ca-listener.pem
    OrganizationalUnitIdentifier: orderer' > ${PWD}/organizations/peerOrganizations/musicx.com/listener.musicx.com/msp/config.yaml

echo "Register listener peer"
echo "=========================="
fabric-ca-client register --caname ca-listener --id.name peer0listener --id.secret peer0pw --id.type peer --tls.certfiles ${PWD}/organizations/fabric-ca/listener/tls-cert.pem
sleep 5

echo "Register listener user"
echo "=========================="
fabric-ca-client register --caname ca-listener --id.name user1 --id.secret user1pw --id.type client --tls.certfiles ${PWD}/organizations/fabric-ca/listener/tls-cert.pem
sleep 5

echo "Register listener Org admin"
echo "=========================="
fabric-ca-client register --caname ca-listener --id.name listeneradmin --id.secret listeneradminpw --id.type admin --tls.certfiles ${PWD}/organizations/fabric-ca/listener/tls-cert.pem
sleep 5

echo "Generate listener peer MSP"
echo "=============================="
fabric-ca-client enroll -u https://peer0listener:peer0pw@localhost:8054 --caname ca-listener -M ${PWD}/organizations/peerOrganizations/musicx.com/listener.musicx.com/peers/peer0.listener.musicx.com/msp --csr.hosts peer0.listener.musicx.com --tls.certfiles ${PWD}/organizations/fabric-ca/listener/tls-cert.pem
sleep 5

echo "Generate listener Peer tls cert"
echo "==================================="
fabric-ca-client enroll -u https://peer0listener:peer0pw@localhost:8054 --caname ca-listener -M ${PWD}/organizations/peerOrganizations/musicx.com/listener.musicx.com/peers/peer0.listener.musicx.com/tls --enrollment.profile tls --csr.hosts peer0.listener.musicx.com --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/listener/tls-cert.pem
sleep 5

echo "Organizing the folders"
echo "======================"

echo "Copy the certificate files"
echo "=========================="

cp organizations/peerOrganizations/musicx.com/listener.musicx.com/peers/peer0.listener.musicx.com/tls/tlscacerts/* organizations/peerOrganizations/musicx.com/listener.musicx.com/peers/peer0.listener.musicx.com/tls/ca.crt
cp organizations/peerOrganizations/musicx.com/listener.musicx.com/peers/peer0.listener.musicx.com/tls/signcerts/* organizations/peerOrganizations/musicx.com/listener.musicx.com/peers/peer0.listener.musicx.com/tls/server.crt
cp organizations/peerOrganizations/musicx.com/listener.musicx.com/peers/peer0.listener.musicx.com/tls/keystore/* organizations/peerOrganizations/musicx.com/listener.musicx.com/peers/peer0.listener.musicx.com/tls/server.key

mkdir -p ${PWD}/organizations/peerOrganizations/musicx.com/listener.musicx.com/msp/tlscacerts
cp ${PWD}/organizations/peerOrganizations/musicx.com/listener.musicx.com/peers/peer0.listener.musicx.com/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/musicx.com/listener.musicx.com/msp/tlscacerts/ca.crt

mkdir -p ${PWD}/organizations/peerOrganizations/musicx.com/listener.musicx.com/tlsca
cp ${PWD}/organizations/peerOrganizations/musicx.com/listener.musicx.com/peers/peer0.listener.musicx.com/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/musicx.com/listener.musicx.com/tlsca/tls-localhost-8054-ca-listener.pem

mkdir -p ${PWD}/organizations/peerOrganizations/musicx.com/listener.musicx.com/ca
cp ${PWD}/organizations/peerOrganizations/musicx.com/listener.musicx.com/peers/peer0.listener.musicx.com/msp/cacerts/* ${PWD}/organizations/peerOrganizations/musicx.com/listener.musicx.com/ca/localhost-8054-ca-listener.pem

cp organizations/peerOrganizations/musicx.com/listener.musicx.com/msp/config.yaml organizations/peerOrganizations/musicx.com/listener.musicx.com/peers/peer0.listener.musicx.com/msp/config.yaml

echo "Generate User MSP"
echo "================="

fabric-ca-client enroll -u https://user1:user1pw@localhost:8054 --caname ca-listener -M ${PWD}/organizations/peerOrganizations/musicx.com/listener.musicx.com/users/User1@listener.musicx.com/msp --tls.certfiles ${PWD}/organizations/fabric-ca/listener/tls-cert.pem
sleep 5

cp organizations/peerOrganizations/musicx.com/listener.musicx.com/msp/config.yaml organizations/peerOrganizations/musicx.com/listener.musicx.com/users/User1@listener.musicx.com/msp/config.yaml

echo "Generate org Admin MSP"
echo "======================"

fabric-ca-client enroll -u https://listeneradmin:listeneradminpw@localhost:8054 --caname ca-listener -M ${PWD}/organizations/peerOrganizations/musicx.com/listener.musicx.com/users/Admin@listener.musicx.com/msp --tls.certfiles ${PWD}/organizations/fabric-ca/listener/tls-cert.pem
sleep 5

cp ${PWD}/organizations/peerOrganizations/musicx.com/listener.musicx.com/msp/config.yaml ${PWD}/organizations/peerOrganizations/musicx.com/listener.musicx.com/users/Admin@listener.musicx.com/msp/config.yaml


echo "==========================================================End of listener ========================================================================================================"

echo "==========================================================Orderer Config ========================================================================================================================"


echo "enroll Orderer ca"
echo "================="

mkdir -p organizations/ordererOrganizations/musicx.com/orderer.musicx.com/


export FABRIC_CA_CLIENT_HOME=${PWD}/organizations/ordererOrganizations/musicx.com/orderer.musicx.com/

fabric-ca-client enroll -u https://admin:adminpw@localhost:9052 --caname ca-orderer --tls.certfiles ${PWD}/organizations/fabric-ca/orderer/tls-cert.pem
sleep 5
echo "Create config.yaml for Orderer "
echo "==================================="

  echo 'NodeOUs:
  Enable: true
  ClientOUIdentifier:
    Certificate: cacerts/localhost-9052-ca-orderer.pem
    OrganizationalUnitIdentifier: client
  PeerOUIdentifier:
    Certificate: cacerts/localhost-9052-ca-orderer.pem
    OrganizationalUnitIdentifier: peer
  AdminOUIdentifier:
    Certificate: cacerts/localhost-9052-ca-orderer.pem
    OrganizationalUnitIdentifier: admin
  OrdererOUIdentifier:
    Certificate: cacerts/localhost-9052-ca-orderer.pem
    OrganizationalUnitIdentifier: orderer' > ${PWD}/organizations/ordererOrganizations/musicx.com/orderer.musicx.com/msp/config.yaml

echo "Register orderer"
echo "=========================="
fabric-ca-client register --caname ca-orderer --id.name orderer0 --id.secret orderer0pw --id.type orderer --tls.certfiles ${PWD}/organizations/fabric-ca/orderer/tls-cert.pem

echo "Register orderer Org admin"
echo "=========================="
fabric-ca-client register --caname ca-orderer --id.name ordereradmin --id.secret ordereradminpw --id.type admin --tls.certfiles ${PWD}/organizations/fabric-ca/orderer/tls-cert.pem

echo "Generate Orderer MSP"
echo "=============================="
fabric-ca-client enroll -u https://orderer0:orderer0pw@localhost:9052 --caname ca-orderer -M ${PWD}/organizations/ordererOrganizations/musicx.com/orderer.musicx.com/orderers/orderer.musicx.com/msp --csr.hosts orderer.musicx.com --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/orderer/tls-cert.pem

cp ${PWD}/organizations/ordererOrganizations/musicx.com/orderer.musicx.com/msp/config.yaml ${PWD}/organizations/ordererOrganizations/musicx.com/orderer.musicx.com/orderers/orderer.musicx.com/msp/config.yaml

echo "Generate Orderer TLS certificates"
echo "==================================="
fabric-ca-client enroll -u https://orderer0:orderer0pw@localhost:9052 --caname ca-orderer -M ${PWD}/organizations/ordererOrganizations/musicx.com/orderer.musicx.com/orderers/orderer.musicx.com/tls --enrollment.profile tls --csr.hosts orderer.musicx.com --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/orderer/tls-cert.pem
sleep 5

echo "Organizing the folders"
echo "======================"
echo "Copy the certificate files"
echo "=========================="

  cp ${PWD}/organizations/ordererOrganizations/musicx.com/orderer.musicx.com/orderers/orderer.musicx.com/tls/tlscacerts/* ${PWD}/organizations/ordererOrganizations/musicx.com/orderer.musicx.com/orderers/orderer.musicx.com/tls/ca.crt
  cp ${PWD}/organizations/ordererOrganizations/musicx.com/orderer.musicx.com/orderers/orderer.musicx.com/tls/signcerts/* ${PWD}/organizations/ordererOrganizations/musicx.com/orderer.musicx.com/orderers/orderer.musicx.com/tls/server.crt
  cp ${PWD}/organizations/ordererOrganizations/musicx.com/orderer.musicx.com/orderers/orderer.musicx.com/tls/keystore/* ${PWD}/organizations/ordererOrganizations/musicx.com/orderer.musicx.com/orderers/orderer.musicx.com/tls/server.key

  mkdir -p ${PWD}/organizations/ordererOrganizations/musicx.com/orderer.musicx.com/orderers/orderer.musicx.com/msp/tlscacerts
  cp ${PWD}/organizations/ordererOrganizations/musicx.com/orderer.musicx.com/orderers/orderer.musicx.com/tls/tlscacerts/* ${PWD}/organizations/ordererOrganizations/musicx.com/orderer.musicx.com/orderers/orderer.musicx.com/msp/tlscacerts/tlsca.musicx.com-cert.pem

  mkdir -p ${PWD}/organizations/ordererOrganizations/musicx.com/orderer.musicx.com/msp/tlscacerts
  cp ${PWD}/organizations/ordererOrganizations/musicx.com/orderer.musicx.com/orderers/orderer.musicx.com/tls/tlscacerts/* ${PWD}/organizations/ordererOrganizations/musicx.com/orderer.musicx.com/msp/tlscacerts/tlsca.musicx.com-cert.pem

  cp ${PWD}/organizations/ordererOrganizations/musicx.com/orderer.musicx.com/msp/config.yaml ${PWD}/organizations/ordererOrganizations/musicx.com/orderer.musicx.com/orderers/orderer.musicx.com/msp/config.yaml

echo "Generate Orderer admin MSP"
echo "=========================="
  fabric-ca-client enroll -u https://ordereradmin:ordereradminpw@localhost:9052 --caname ca-orderer -M ${PWD}/organizations/ordererOrganizations/musicx.com/orderer.musicx.com/users/Admin@musicx.com/msp --tls.certfiles ${PWD}/organizations/fabric-ca/orderer/tls-cert.pem
sleep 5

  cp ${PWD}/organizations/ordererOrganizations/musicx.com/orderer.musicx.com/msp/config.yaml ${PWD}/organizations/ordererOrganizations/musicx.com/orderer.musicx.com/users/Admin@musicx.com/msp/config.yaml

echo "=========================================================End of Orderer ========================================================================================================================"







