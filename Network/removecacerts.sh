#!/bin/bash


sudo rm -rf \
    ./organizations/fabric-ca/listener/msp \
    ./organizations/fabric-ca/listener/ca-cert.pem \
    ./organizations/fabric-ca/listener/fabric-ca-server.db \
    ./organizations/fabric-ca/listener/IssuerPublicKey \
    ./organizations/fabric-ca/listener/IssuerRevocationPublicKey \
    ./organizations/fabric-ca/listener/tls-cert.pem

sudo rm -rf \
    ./organizations/fabric-ca/artist/msp \
    ./organizations/fabric-ca/artist/ca-cert.pem \
    ./organizations/fabric-ca/artist/fabric-ca-server.db \
    ./organizations/fabric-ca/artist/IssuerPublicKey \
    ./organizations/fabric-ca/artist/IssuerRevocationPublicKey \
    ./organizations/fabric-ca/artist/tls-cert.pem

sudo rm -rf \
    ./organizations/fabric-ca/orderer/msp \
    ./organizations/fabric-ca/orderer/ca-cert.pem \
    ./organizations/fabric-ca/orderer/fabric-ca-server.db \
    ./organizations/fabric-ca/orderer/IssuerPublicKey \
    ./organizations/fabric-ca/orderer/IssuerRevocationPublicKey \
    ./organizations/fabric-ca/orderer/tls-cert.pem

sudo rm -rf \
    ./organizations/ordererOrganizations/musicx.com/orderer.musicx.com/msp \
    ./organizations/ordererOrganizations/musicx.com/orderer.musicx.com/fabric-ca-client-config.yaml \
    ./organizations/ordererOrganizations/musicx.com/orderer.musicx.com/orderers \
    ./organizations/ordererOrganizations/musicx.com/orderer.musicx.com/users
    
sudo rm -rf \
    ./organizations/peerOrganizations/musicx.com/artist.musicx.com/ca \
    ./organizations/peerOrganizations/musicx.com/artist.musicx.com/msp \
    ./organizations/peerOrganizations/musicx.com/artist.musicx.com/peers \
    ./organizations/peerOrganizations/musicx.com/artist.musicx.com/tlsca \
    ./organizations/peerOrganizations/musicx.com/artist.musicx.com/users \
    ./organizations/peerOrganizations/musicx.com/artist.musicx.com/fabric-ca-client-config.yaml

sudo rm -rf \
    ./organizations/peerOrganizations/musicx.com/listener.musicx.com/ca \
    ./organizations/peerOrganizations/musicx.com/listener.musicx.com/msp \
    ./organizations/peerOrganizations/musicx.com/listener.musicx.com/peers \
    ./organizations/peerOrganizations/musicx.com/listener.musicx.com/tlsca \
    ./organizations/peerOrganizations/musicx.com/listener.musicx.com/users \
    ./organizations/peerOrganizations/musicx.com/listener.musicx.com/fabric-ca-client-config.yaml

sudo rm -rf \
    ./organizations/peerOrganizations/musicx.com/mvd.musicx.com/ca \
    ./organizations/peerOrganizations/musicx.com/mvd.musicx.com/msp \
    ./organizations/peerOrganizations/musicx.com/mvd.musicx.com/peers \
    ./organizations/peerOrganizations/musicx.com/mvd.musicx.com/tlsca \
    ./organizations/peerOrganizations/musicx.com/mvd.musicx.com/users \
    ./organizations/peerOrganizations/musicx.com/mvd.musicx.com/fabric-ca-client-config.yaml