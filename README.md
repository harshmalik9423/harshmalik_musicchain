# Music Blockchain Network with GUI client

## Setup HLF Network
1. Using createCa.sh, get certificates for different members in both orgs i.e. Artist and Listeners.
2. Using startNetwork.sh :-
    i. Generate Genesis Block
    ii. Create **musicchannel** channel
    iii. Start the docker containers and join artist and listener peers to the n/w
    iv. Generate Anchor peers for the orgs and update this on the n/w
    v. Package the chaincode and install it on both org's peers

3. Approve the chaincode from both orgs and commit it
4. Invoke the chaincode from the peers

## Setting up Fabric Client
1. Enter **npm install** in Client directory.
2. For both artist and listener, run **node _filename_** in following sequence:
    i. node enrollAdmin.js
    ii. node registerUser.js
    iii. Query the chaincode

## Setting up Fabric Explorer
1. Go to the the explorer directory and enter **docker-compose up** to bring db and explorer containers up
2. In the browser, go to **localhost:8080** and login with:-
    Username: exploreradmin
    Password: exploreradminpw

## Setting up GUI client and REST API
1. Go to RestAPI directory and enter **npm install**
2. Enter **npm start**
3. Go to **localhost:3000** to access the GUI for submitting create transaction.
4. If using Postman, send the requests to endpoint mentioned in index.js for reference on payload.