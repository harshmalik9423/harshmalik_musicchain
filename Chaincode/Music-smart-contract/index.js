/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const SongContract = require('./lib/song-contract');
const MusicContract = require('./lib/music-contract');

module.exports.SongContract = SongContract;
module.exports.MusicContract = MusicContract;
module.exports.contracts = [ SongContract, MusicContract ];
