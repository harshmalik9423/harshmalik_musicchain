/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { ChaincodeStub, ClientIdentity } = require('fabric-shim');
const { SongContract } = require('..');
const winston = require('winston');

const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

chai.should();
chai.use(chaiAsPromised);
chai.use(sinonChai);

class TestContext {

    constructor() {
        this.stub = sinon.createStubInstance(ChaincodeStub);
        this.clientIdentity = sinon.createStubInstance(ClientIdentity);
        this.logger = {
            getLogger: sinon.stub().returns(sinon.createStubInstance(winston.createLogger().constructor)),
            setLevel: sinon.stub(),
        };
    }

}

describe('SongContract', () => {

    let contract;
    let ctx;

    beforeEach(() => {
        contract = new SongContract();
        ctx = new TestContext();
        // ctx.stub.getState.withArgs('1001').resolves(Buffer.from('{"value":"song 1001 value"}'));
        // ctx.stub.getState.withArgs('1002').resolves(Buffer.from('{"value":"song 1002 value"}'));
    });

    describe('#songExists', () => {

        it('should return true for a song', async () => {
            await contract.songExists(ctx, '001').should.eventually.be.true;
        });

        it('should return false for a song that does not exist', async () => {
            await contract.songExists(ctx, '002').should.eventually.be.false;
        });

    });

    // describe('#createSong', () => {

    //     it('should create a song', async () => {
    //         await contract.createSong(ctx, '1003', 'song 1003 value');
    //         ctx.stub.putState.should.have.been.calledOnceWithExactly('1003', Buffer.from('{"value":"song 1003 value"}'));
    //     });

    //     it('should throw an error for a song that already exists', async () => {
    //         await contract.createSong(ctx, '1001', 'myvalue').should.be.rejectedWith(/The song 1001 already exists/);
    //     });

    // });

    describe('#readSong', () => {

        it('should return a song', async () => {
            await contract.readSong(ctx, '001').should.eventually.deep.equal({ songName:"paradise", songArtist:"coldplay",songAlbum:"Mylo Xyloto"});
        });

        it('should throw an error for a song that does not exist', async () => {
            await contract.readSong(ctx, '1003').should.be.rejectedWith(/The song 1003 does not exist/);
        });

    });

    // describe('#deleteSong', () => {

    //     it('should delete a song', async () => {
    //         await contract.deleteSong(ctx, '1001');
    //         ctx.stub.deleteState.should.have.been.calledOnceWithExactly('1001');
    //     });

    //     it('should throw an error for a song that does not exist', async () => {
    //         await contract.deleteSong(ctx, '1003').should.be.rejectedWith(/The song 1003 does not exist/);
    //     });

    // });

});
