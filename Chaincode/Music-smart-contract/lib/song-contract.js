/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { Contract } = require('fabric-contract-api');

class SongContract extends Contract {

    async songExists(ctx, songId) {
        const buffer = await ctx.stub.getState(songId);
        return (!!buffer && buffer.length > 0);
    }

    async createSong(ctx, songId, songName, songArtist, songAlbum) {
        const exists = await this.songExists(ctx, songId);
        if (exists) {
            throw new Error(`The song ${songId} already exists`);
        }
        const asset = { songName, songArtist, songAlbum };
        const buffer = Buffer.from(JSON.stringify(asset));
        await ctx.stub.putState(songId, buffer);
        return 'Song registered';
    }

    async readSong(ctx, songId) {
        const exists = await this.songExists(ctx, songId);
        if (!exists) {
            throw new Error(`The song ${songId} does not exist`);
        }
        const buffer = await ctx.stub.getState(songId);
        const asset = JSON.parse(buffer.toString());
        return asset;
    }
    
    async deleteSong(ctx, songId) {
        const exists = await this.songExists(ctx, songId);
        if (!exists) {
            throw new Error(`The song ${songId} does not exist`);
        }
        await ctx.stub.deleteState(songId);
    }

    async queryAllSongs(ctx) {
        const queryString = {
            selector: {
                assetType: 'song',
            }
        };
        let resultIterator = await ctx.stub.getQueryResult(
            JSON.stringify(queryString)
        );
        let result = await this.getAllResults(resultIterator, false);
        return JSON.stringify(result);
    }

    async getSongByRange(ctx, startKey, endKey) {
        let resultIterator = await ctx.stub.getStateByRange(startKey, endKey);
        let result = await this.getAllResults(resultIterator, false);
        return JSON.stringify(result);
    }

    async getMusicWithPagination(ctx, _pageSize, _bookmark) {
        const queryString = {
            selector: {
                assetType: 'song',
            },
        };

        const pageSize = parseInt(_pageSize, 10);
        const bookmark = _bookmark;

        const { iterator, metadata } = await ctx.stub.getQueryResultWithPagination(
            JSON.stringify(queryString),
            pageSize,
            bookmark
        );

        const result = await this.getAllResults(iterator, false);

        const results = {};
        results.Result = result;
        results.ResponseMetaData = {
            RecordCount: metadata.fetched_records_count,
            Bookmark: metadata.bookmark,
        };
        return JSON.stringify(results);
    }

    async getMusicHistory(ctx, musicId) {
        let resultsIterator = await ctx.stub.getHistoryForKey(musicId);
        let results = await this.getAllResults(resultsIterator, true);
        return JSON.stringify(results);
    }

    async getAllResults(iterator, isHistory) {
        let allResult = [];

        for (
            let res = await iterator.next();
            !res.done;
            res = await iterator.next()
        ) {
            if (res.value && res.value.value.toString()) {
                let jsonRes = {};

                if (isHistory && isHistory === true) {
                    jsonRes.TxId = res.value.tx_id;
                    jsonRes.timestamp = res.value.timestamp;
                    jsonRes.Value = JSON.parse(res.value.value.toString());
                } else {
                    jsonRes.Key = res.value.key;
                    jsonRes.Record = JSON.parse(res.value.value.toString());
                }
                allResult.push(jsonRes);
            }
        }
        await iterator.close();
        return allResult;
    }
}

module.exports = SongContract;
