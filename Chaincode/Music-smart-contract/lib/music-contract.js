/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { Contract } = require('fabric-contract-api');
const crypto = require('crypto');


class MusicContract extends Contract {

    async musicExists(ctx, musicId) {
        const data = await ctx.stub.getPrivateDataHash('PrivMusicCollection', musicId);
        return (!!data && data.length > 0);
    }

    async createMusic(ctx, musicId) {
        const exists = await this.musicExists(ctx, musicId);
        if (exists) {
            throw new Error(`The asset music ${musicId} already exists`);
        }

        const transientData = ctx.stub.getTransient();
        if (transientData.size === 0 || 
            !transientData.has('songName') ||
            !transientData.has('songArtist') ||
            !transientData.has('songAlbum')) {
            throw new Error('The privateValue key was not specified in transient data. Please try again.');
        }

        const musicAsset = {};

        musicAsset.songName = transientData.get('songName').toString();
        musicAsset.songArtist = transientData.get('songArtist').toString();
        musicAsset.songAlbum = transientData.get('songAlbum').toString();
        musicAsset.type = 'privMusic';


        await ctx.stub.putPrivateData('PrivMusicCollection', musicId, Buffer.from(JSON.stringify(musicAsset)));
    }

    async readMusic(ctx, musicId) {
        const exists = await this.musicExists(ctx, musicId);
        if (!exists) {
            throw new Error(`The asset music ${musicId} does not exist`);
        }

        const musicData = await ctx.stub.getPrivateData('PrivMusicCollection', musicId);
        privateDataString = JSON.parse(musicData.toString());
        return privateDataString;
    }

    async deleteMusic(ctx, musicId) {
        const exists = await this.musicExists(ctx, musicId);
        if (!exists) {
            throw new Error(`The asset music ${musicId} does not exist`);
        }

        await ctx.stub.deletePrivateData('PrivMusicCollection', musicId);
    }

    async verifyMusic(ctx, mspid, musicId, objectToVerify) {

        // Convert provided object into a hash
        const hashToVerify = crypto.createHash('sha256').update(objectToVerify).digest('hex');
        const pdHashBytes = await ctx.stub.getPrivateDataHash(`_implicit_org_${mspid}`, musicId);
        if (pdHashBytes.length === 0) {
            throw new Error('No private data hash with the key: ' + musicId);
        }

        const actualHash = Buffer.from(pdHashBytes).toString('hex');

        // Compare the hash calculated (from object provided) and the hash stored on public ledger
        if (hashToVerify === actualHash) {
            return true;
        } else {
            return false;
        }
    }


}

module.exports = MusicContract;
