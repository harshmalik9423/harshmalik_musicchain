function SongWriteData(){
    event.preventDefault();
    const songId = document.getElementById('songId').value;
    const songName = document.getElementById('songName').value;
    const songArtist = document.getElementById('songArtist').value;
    const songAlbum = document.getElementById('songAlbum').value;
    const userName = document.getElementById('userName').value;
    console.log(songId+songName+songArtist+songAlbum+userName);

    if (songId.length==0||songName.length==0||songArtist.length==0||songAlbum.length==0||userName.length==0) {
        alert("You Missed Something")
        }
    else{
        fetch('/addSong',{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',              
            },
            body: JSON.stringify({songId: songId, songName: songName, songArtist: songArtist, songAlbum: songAlbum, userName: userName})
        })
        .then(function(response){
            if(response.status == 200) {
                alert("Added Song")
            } else {
                alert("Something Went wrong")
            }

        })
        .catch(function(error){
            alert("Something went wrong")
        })    
    }
}
