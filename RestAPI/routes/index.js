var express = require('express');
var router = express.Router();
const { Gateway, Wallets } = require('fabric-network');
const fs = require('fs');
const path = require('path')
const {clientApplication} = require('./client');
const Joi = require('joi')
var jwt = require('jsonwebtoken');
var {applicationSecret} = require('./utils')


router.get('/', function(req, res, next) {
    res.render('index', { title: 'Artist Dashboard' });
})


/* GET home page. */
router.post('/song',async function(req, res, next) {
 
    let ArtistClient = new clientApplication();

    //songId, songName, songArtist, songAlbum
    var userName = req.body.userName;
    var txName = 'createSong';
    var songId = req.body.songId;
    var songName = req.body.songName;
    var songArtist = req.body.songArtist;
    var songAlbum = req.body.songAlbum;
     
    var decoded = jwtVerify(req)
    
    if(decoded.userName === userName) {

    

    const schema = Joi.object({
        userName: Joi.string().required(),
        songId: Joi.string().required(),
        songName: Joi.string().required(),
        songArtist: Joi.string().required(),
        songAlbum: Joi.string().required(),  
        });
      
      const { error } = schema.validate(req.body);
      if(error) {
        res.status(500).send({ "status": "false", "message": `${error.message}` });
    } else {
    ArtistClient.generatedAndSubmitTxn(
        "artist",
        userName,
        "musicchannel", 
        "harshmusicx",
        "SongContract",
        txName,
        songId,songName,songArtist,songAlbum
      ).then(message => {
          console.log("Message is $$$$",message)
          res.status(200).send({status:true, message: "Added Song"})
        }
      )
      .catch(error =>{
        console.log("Some error Occured $$$$###", error)
        res.status(500).send({status:false, error:`Failed to Add`,message:`${error}`})
      });
    }
} else {
    res.status(401).send("Unauthorized request")
}

});

router.get('/song', async function(req, res, next) {
    
    var songId = req.query.id;
    var userName = req.query.userName
    let ArtistClient = new clientApplication();
    
    var decoded = jwtVerify(req)

    if(decoded.userName === userName) {
    ArtistClient.generatedAndEvaluateTxn( 
        "artist",
        userName,
        "musicchannel", 
        "harshmusicx",
        "SongContract",
        "readSong", songId)
        .then(message => {
          
          res.status(200).send({status:true, Cardata : message.toString() });
        }).catch(error =>{
         
          res.status(500).send({status: false, error:`Failed to Add`,message:`${error}`})
        });
    } else {
        res.status(401).send("Unauthorized request")
    }
})

//For UI
router.post('/addSong',async function(req, res, next) {
 
    let ArtistClient = new clientApplication();

    //songId,songName,songArtist,songAlbum
    var userName = req.body.userName;
    var txName = 'createSong';
    var songId = req.body.songId;
    var songName = req.body.songName;
    var songArtist = req.body.songArtist;
    var songAlbum = req.body.songAlbum;
    
    const schema = Joi.object({
        userName: Joi.string().required(),
        songId: Joi.string().required(),
        songName: Joi.string().required(),
        songArtist: Joi.string().required(),
        songAlbum: Joi.string().required(),   
        });
      
      const { error } = schema.validate(req.body);
      if(error) {
        res.status(500).send({ "status": "false", "message": `${error.message}` });
    } else {
    ArtistClient.generatedAndSubmitTxn(
        "artist",
        userName,
        "musicchannel", 
        "harshmusicx",
        "SongContract",
        txName,
        songId,songName,songArtist,songAlbum
      ).then(message => {
          console.log("Message is $$$$",message)
          res.status(200).send({status:true, message: "Added Car"})
        }
      )
      .catch(error =>{
        console.log("Some error Occured $$$$###", error)
        res.status(500).send({status:false, error:`Failed to Add`,message:`${error}`})
      });
    }


});




const jwtVerify = (req) => {
    var token = req.headers.authorization;
    var jwtToken = token ? token.split(' ')[1] : ''
    try {
        var decoded = jwt.verify(jwtToken, applicationSecret);
        return decoded
      } catch(err) {

        return "error"
      }

}
//http://localhost:3000/car?id=01&&userName=appUser
    

module.exports = router;
